import React from 'react'
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { GET_MOVIE } from '../query';
import MovieDetail from '../components/MovieDetail'
import EditMovie from '../components/EditMovie'
import LoadingPage from '../components/LoadingPage';

export default function Detail() {
  const params = useParams()
  const {loading, error, data, refetch} = useQuery(GET_MOVIE, {
    variables: { id: params.id }
  })

  if(loading) return <LoadingPage />
  if(error)  {
    console.log(error)
    return <div>{error.message}</div>
  }

  return (
    <div className="container content">
      <div className="row">
        <div className="col-4">
          <img className="image-poster" src={data.movie.poster_path} alt="movie-poster"/>
        </div>
        <div className="col">
          <ul className="nav nav-tabs nav-fill mt-4" id="myTab" role="tablist">
            <li className="nav-item">
              <a className="nav-link active white-text" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                aria-selected="true">Movie Information</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                aria-selected="false">Edit Movie</a>
            </li>
          </ul>
          <div className="tab-content" id="myTabContent">
            <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <MovieDetail movie={data.movie}/>
            </div>
            <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <EditMovie movie={data.movie} refetch={refetch} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}