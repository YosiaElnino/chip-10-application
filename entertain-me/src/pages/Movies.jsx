import React from 'react'
import { useQuery, useMutation } from '@apollo/client';
import { DELETE_MOVIE, GET_FAVORITES, GET_MOVIES } from '../query'
import { useHistory } from "react-router-dom";
import client from '../config/graphql';
import LoadingPage from '../components/LoadingPage';
import ErrorPage from '../components/ErrorPage';
import EmptyPage from '../components/EmptyPage';

export default function Movies() {
  const {loading, error, data} = useQuery(GET_MOVIES)
  const [ deleteMovie ] = useMutation(DELETE_MOVIE, {
    refetchQueries: [
      { query: GET_MOVIES}
    ]
  })
  const history = useHistory()

  const deleteThisMovie = (id) => {
    deleteMovie({
      variables: {id}
    })
  }

  const openDetail = (id) => {
    history.push(`/movies/${id}`)
  }

  const addToFavorite = (movie) => {
    const newFavorite = {
      _id: movie._id,
      title: movie.title,
      overview: movie.overview,
      poster_path: movie.poster_path,
      popularity: movie.popularity,
      tags: movie.tags
    }
    const { favorites: currentFavorite } = client.readQuery({
      query: GET_FAVORITES
    })

    client.writeQuery({
      query: GET_FAVORITES,
      data: {
        favorites: [...currentFavorite, newFavorite]
      }
    })
  }

  if(loading) return <LoadingPage />
  if(error) return <ErrorPage />
  if(!data.movies?.length) return <EmptyPage />
  
  return (
    <div className="content d-flex flex-column justify-content-center">
      <h1>Movies</h1>
      <div className="align-self-center cardlist-container d-flex flex-wrap p-2">
        {
          data.movies?.map(movie => {
            return (
              <div key={movie._id} className="card movie-card-favorite">
                <div className="d-flex justify-content-end">
                  <i onClick={() => addToFavorite(movie)} className="close fas fa-heart text-right mr-3 mt-3"></i>
                  <i onClick={() => deleteThisMovie(movie._id)} className="close fas fa-trash text-right mr-3 mt-3"></i>
                </div>
                <div onClick={() => openDetail(movie._id)} className="card-body waves-effect">
                  <div className="row">
                    <div className="col">
                      <img className="movie-img hoverable" src={movie.poster_path} alt="movie-img"/>
                    </div>
                    <div className="col">
                      <h4 className="card-title text-left">
                        <strong>{movie.title}</strong>
                      </h4>
                      <h5 className="text-left"><i className="fa fa-star star-icon"></i> {movie.popularity}</h5>
                    </div>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}