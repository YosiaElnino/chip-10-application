import React from 'react'
import Jumbotron from '../components/Jumbotron'
import HomeCard from '../components/HomeCard'


export default function Home() {
  return (
    <>
      <Jumbotron />
      <HomeCard />
    </>
  )
}