import React, { useState, useEffect } from 'react';
import client from '../config/graphql';
import { GET_FAVORITES } from '../query'
import LoadingPage from '../components/LoadingPage';
import EmptyPage from '../components/EmptyPage';

export default function Favorite() {
  const [favorites, setFavorites] = useState('')
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    const cache = client.readQuery({
      query: GET_FAVORITES
    })
    setFavorites(cache)
    setLoading(false)
  }, [])

  const deleteFavorite = (id) => {
    const { favorites: currentFavorite } = client.readQuery({
      query: GET_FAVORITES
    })

    client.writeQuery({
      query: GET_FAVORITES,
      data: {
        favorites: currentFavorite.filter(el => el._id !== id)
      }
    })

    const cache = client.readQuery({
      query: GET_FAVORITES
    })

    setFavorites(cache)
  }

  if(isLoading) return <LoadingPage />
  if(!favorites.favorites?.length) return <EmptyPage />

  return (
    <div className="content d-flex flex-column justify-content-center">
      <h1>Favorites</h1>
      <div className="align-self-center cardlist-container d-flex flex-wrap p-2">
        {
          favorites.favorites?.map(movie => {
            return (
              <div key={movie._id} className="card movie-card-favorite">
                <i onClick={() => deleteFavorite(movie._id)} className="close fas fa-trash text-right mr-3 mt-3"></i>
                <div className="card-body waves-effect">
                  <div className="row">
                    <div className="col">
                      <img className="movie-img hoverable" src={movie.poster_path} alt="movie-img"/>
                    </div>
                    <div className="col">
                      <h4 className="card-title text-left">
                        <strong>{movie.title}</strong>
                      </h4>
                      <h5 className="text-left"><i className="fa fa-star star-icon"></i> {movie.popularity}</h5>
                    </div>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}