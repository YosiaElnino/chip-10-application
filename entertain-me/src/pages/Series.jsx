import React from 'react'
import { useQuery } from '@apollo/client';
import { GET_SERIES } from '../query'
import LoadingPage from '../components/LoadingPage';
import ErrorPage from '../components/ErrorPage';
import EmptyPage from '../components/EmptyPage';

export default function Series() {
  const {loading, error, data} = useQuery(GET_SERIES)

  if(loading) return <LoadingPage />
  if(error) return <ErrorPage />
  if(!data.series?.length) return <EmptyPage />
  return (
    <div className="content d-flex flex-column justify-content-center">
      <h1>Series</h1>
      <div className="align-self-center cardlist-container d-flex flex-wrap p-2">
        {
          data.series?.map(movie => {
            return (
              <div key={movie._id} className="card movie-card-favorite">
                <div className="card-body waves-effect">
                  <div className="row">
                    <div className="col">
                      <img className="movie-img hoverable" src={movie.poster_path} alt="movie-img"/>
                    </div>
                    <div className="col">
                      <h4 className="card-title text-left">
                        <strong>{movie.title}</strong>
                      </h4>
                      <h5 className="text-left"><i className="fa fa-star star-icon"></i> {movie.popularity}</h5>
                    </div>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}