import React, { useState } from 'react'
import addIllustration from '../assets/add-movie.svg'
import { useMutation } from '@apollo/client';
import { ADD_MOVIE, GET_DATA } from '../query'
import { useHistory } from "react-router-dom";


export default function AddMovie() {
  const [title, setTitle] = useState('')
  const [allFilled, setFilled] = useState(true)
  const [poster_path, setPoster] = useState('')
  const [overview, setOverview] = useState('')
  const [popularity, setPopularity] = useState('')
  const [tag, setTag] = useState('')
  const [tags, setTags] = useState([])
  const [ addMovie] = useMutation(ADD_MOVIE, {
    refetchQueries: [
      { query: GET_DATA}
    ]
  })
  const history = useHistory()

  const handleTitleChange = (event) => {
    event.preventDefault()
    setTitle(event.target.value)
  }

  const handleOverviewChange = (event) => {
    event.preventDefault()
    setOverview(event.target.value)
  }

  const handlePosterChange = (event) => {
    event.preventDefault()
    setPoster(event.target.value)
  }

  const handlePopularityChange = (event) => {
    event.preventDefault()
    setPopularity(event.target.value)
  }

  const handleTagChange = (event) => {
    event.preventDefault()
    setTag(event.target.value)
  }

  const addTag = (event) => {
    event.preventDefault()
    const newTags = tags.map(el => el)
    newTags.push(tag)
    setTags(newTags)
    setTag('')
  }

  const deleteTag = (value) => {
    const newTags = tags.map(el => el)
    const filtered = newTags.filter(el => el !== value)
    setTags(filtered)
  }

  const validate = () => {
    if (!title || !overview || !poster_path || !popularity || !tags.length) {
      setFilled(false)
      return false
    } else {
      setFilled(true)
      return true
    }
  }

  const renderError = () => {
    if (!allFilled) return (
      <p className="error-form">All sections of the form cannot be empty</p>
    )
  }

  const submit = (event) => {
    event.preventDefault()
    if (validate()) {
      const input = {
        title,
        overview,
        poster_path,
        popularity: Number(popularity),
        tags
      }
      addMovie({ 
        variables: { movie: input }
      })
      setTitle('')
      setOverview('')
      setPoster('')
      setPopularity('')
      setTags([])
      history.push('/')
    }
  }

  return (
    <>
      <div className="d-flex justify-content-center">
        <div className="card add-card">
          <div className="card-body">
            <h1>Add Movie</h1>
            <img src={addIllustration} alt="add-movie" className="add-illustration"/>
            <form className="text-center p-5" action="#!">
                {renderError()}
                <input type="text" className="form-control mb-3" placeholder="Title" value={title} onChange={event => handleTitleChange(event)}></input>
                <div className="form-group">
                  <textarea className="form-control rounded-0" rows="2" placeholder="Overview" value={overview} onChange={event => handleOverviewChange(event)}></textarea>
                </div>
                <input type="text" className="form-control mb-3" placeholder="Poster Path" value={poster_path} onChange={event => handlePosterChange(event)}></input>
                <input type="text" className="form-control mb-3" placeholder="Popularity" value={popularity} onChange={event => handlePopularityChange(event)}></input>
                <div className="input-group mb-3">
                  <input type="text" className="form-control" placeholder="Tags" value={tag} onChange={event => handleTagChange(event)}></input>
                  <div className="input-group-append">
                    <button onClick={(event) => addTag(event)} className="add-tag">Add Tag</button>
                  </div>
                </div>
                <div className="chip-container d-flex flex-wrap mb-3">
                  {
                    tags?.map((el, index) => {
                      return (
                          <div key={index} className="chip tag-chip p-2">
                            {el}
                            <i onClick={() => deleteTag(el)} className="close fas fa-times"></i>
                          </div>
                      )
                    })
                  }
                </div>
                <button onClick={(event) => submit(event)} className="btn btn-submit btn-block">Add Movie</button>
            </form>
          </div>
        </div>
      </div>
    </>
  )
}