export { default as Home } from './Home';
export { default as Movies } from './Movies';
export { default as Detail } from './Detail';
export { default as Favorite } from './Favorite';
export { default as Series } from './Series';
export { default as Add } from './AddMovie';
