import './App.css';
import NavBar from './components/NavBar'
import { Switch } from 'react-router-dom';
import { Home, Movies, Series, Detail, Favorite, Add } from './pages';
import { init as initApm } from "@elastic/apm-rum"
import { ApmRoute } from '@elastic/apm-rum-react'

const apm = initApm({
  serviceName: "entertain-me",
  serverUrl:
    "http://apm-server:8200",
  serviceVersion: "1.0"
});

function App() {
  return (
    <div className="App">
      <NavBar />
      <Switch>
        <ApmRoute exact path = '/'>
          <Home />
        </ApmRoute>
        <ApmRoute exact path = '/movies'>
          <Movies />
        </ApmRoute>
        <ApmRoute path = '/favorites'>
          <Favorite />
        </ApmRoute>
        <ApmRoute path = '/movies/:id'>
          <Detail />
        </ApmRoute>
        <ApmRoute path = '/add'>
          <Add />
        </ApmRoute>
        <ApmRoute path = '/series'>
          <Series />
        </ApmRoute>
      </Switch>
    </div>
  );
}

export default App;
