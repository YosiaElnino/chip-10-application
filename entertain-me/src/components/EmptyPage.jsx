import React from 'react';
import emptyIllustration from '../assets/no-data.svg'

export default function EmptyPage () {
  return (
    <>
      <div className="content d-flex flex-column justify-content-center loading-page">
        <img className="error-illustration align-self-center" src={emptyIllustration} alt="error"/>
        <h1 className="error-msg">No data to be shown</h1>
      </div>
    </>
  )
}