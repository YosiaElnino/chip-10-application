import React from 'react';

export default function LoadingPage () {
  return (
    <>
      <div className="d-flex justify-content-center loading-page">
        <div className="loading-spinner text-center spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    </>
  )
}