import React from 'react';
import { useMutation } from '@apollo/client';
import { DELETE_MOVIE, GET_DATA, GET_FAVORITES } from '../query';
import { useHistory } from "react-router-dom";
import client from '../config/graphql';

export default function MovieDetail(props) {
  const movie = props.movie
  const [ deleteMovie ] = useMutation(DELETE_MOVIE, {
    refetchQueries: [
      { query: GET_DATA}
    ]
  })
  const history = useHistory()

  const deleteThisMovie = () => {
    deleteMovie({
      variables: {id: props.movie._id}
    })
    history.push('/')
  }

  const addToFavorite = () => {
    const newFavorite = {
      _id: props.movie._id,
      title: props.movie.title,
      overview: props.movie.overview,
      poster_path: props.movie.poster_path,
      popularity: props.movie.popularity,
      tags: props.movie.tags
    }
    const { favorites: currentFavorite } = client.readQuery({
      query: GET_FAVORITES
    })

    client.writeQuery({
      query: GET_FAVORITES,
      data: {
        favorites: [...currentFavorite, newFavorite]
      }
    })
  }
  
  return (
    <>
      <div className="d-flex justify-content-between">
        <h1 className="text-left">{movie.title}</h1>
        <button onClick={() => addToFavorite()} type="button" className="btn btn-favorites waves-effect"><i className="fa fa-heart mr-3"></i>Add to Favorite</button>
      </div>
      <h3 className="text-left"><i className="fa fa-star star-icon"></i> {movie.popularity}</h3>
      <h5 className="text-muted text-left">"{movie.overview}"</h5>
      <h4 className="text-left mt-5">Tags:</h4>
      <div className="chip-container d-flex flex-wrap">
        {
          movie.tags?.map((el, index) => {
            return (
              <div key={'tag'+index} className="chip tag-chip p-2">
                {el}
              </div>
            )
          })
        }
      </div>
      <button onClick={() => deleteThisMovie()} className="btn btn-submit btn-block mt-5">Delete Movie</button>
    </>
  )
}