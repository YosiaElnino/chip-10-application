import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_DATA } from '../query'
import { useHistory } from "react-router-dom";
import LoadingPage from '../components/LoadingPage';

export default function HomeCard() {
  const {loading, error, data} = useQuery(GET_DATA)
  const history = useHistory()

  const openDetail = (id) => {
    history.push(`/movies/${id}`)
  }

  if(loading) return <LoadingPage />
  if(error)  {
    console.log(error)
    return <div>{error.message}</div>
  }

  return (
    <>
      <div className="outer-container d-lg-block d-md-none d-sm-none d-none">
        <div className="movie-card-container">
          {
            data.movies?.map(movie => {
              return (
                <div key={movie._id} className="card movie-card waves-effect" onClick={() => openDetail(movie._id)}>
                  <div className="card-body">
                    <div className="row">
                      <div className="col">
                        <img className="movie-img hoverable" src={movie.poster_path} alt="movie-img"/>
                      </div>
                      <div className="col">
                        <h4 className="card-title text-left">
                          <strong>{movie.title}</strong>
                        </h4>
                        <h5 className="text-left"><i className="fa fa-star star-icon"></i> {movie.popularity}</h5>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          }
          {
            data.series?.map(el => {
              return (
                <div key={el._id} className="card movie-card">
                  <div className="card-body">
                    <div className="row">
                      <div className="col">
                        <img className="movie-img hoverable" src={el.poster_path} alt="series-img"/>
                      </div>
                      <div className="col">
                        <h4 className="card-title text-left">
                          <strong>{el.title}</strong>
                        </h4>
                        <h5 className="text-left"><i className="fa fa-star star-icon"></i> {el.popularity}</h5>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    </>
  );
}