import React from "react";
import { NavLink } from 'react-router-dom';

function Navbar () {
  return (
    <nav className="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar" style={{backgroundColor: "#3E2089"}}>
      <NavLink to="/" exact={true} className="navbar-brand"><strong>Entertain</strong> Me</NavLink>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
            <li className="nav-item">
                <NavLink to='/movies' activeStyle={{fontWeight: "bold"}} className="nav-link">Movies</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to="/series" activeStyle={{fontWeight: "bold"}} className="nav-link">Series</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to="/favorites" activeStyle={{fontWeight: "bold"}} className="nav-link">Favorites</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to="/add" activeStyle={{fontWeight: "bold"}} className="nav-link">Add Movie</NavLink>
            </li>
        </ul>
      </div>
    </nav>
  )
}

export default Navbar