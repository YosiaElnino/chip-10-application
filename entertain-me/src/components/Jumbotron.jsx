import React from 'react';
import illustration from '../assets/entertain-me.svg';
import { useHistory } from "react-router-dom";

export default function Jumbotron() {
  const history = useHistory()

  const goToMovie = () => {
    history.push('/movies')
  }

  const goToSeries = () => {
    history.push('/series')
  }

  return (
    <div className="jumbotron-home" style={{width: '100vw', height: '85vh', backgroundColor: '#3E2089'}}>
      <div className="row">
        <div className="col">
          <img src={illustration} alt="illustration" className="illustration"/>
        </div>
        <div className="col">
          <h1 className="title-page">Entertain Me</h1>
          <h2 className="tagline">"Your daily entertainment"</h2>
          <button onClick={() => goToMovie()} type="button" className="btn btn-outlined waves-effect">View Movies</button>
          <button onClick={() => goToSeries()} type="button" className="btn btn-outlined waves-effect">View Series</button>
        </div>
      </div>
    </div>
  )
}