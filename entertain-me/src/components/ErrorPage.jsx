import React from 'react';
import errorIllustration from '../assets/error.svg'

export default function ErrorPage () {
  return (
    <>
      <div className="content d-flex flex-column justify-content-center loading-page">
        <img className="error-illustration align-self-center" src={errorIllustration} alt="error"/>
        <h1 className="error-msg">Oops.. something went wrong</h1>
      </div>
    </>
  )
}