import { gql } from '@apollo/client'

export const GET_DATA = gql`
  query getData{
    movies{    
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }

    series{
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }
  }
`

export const GET_MOVIES = gql`
  query getMovies{
    movies{    
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }
  }
`

export const GET_SERIES = gql`
  query getSeries{
    series{    
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }
  }
`

export const ADD_MOVIE = gql`
  mutation AddMovie($movie: newMovie) {
    addMovie(movie: $movie) {
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }
  }
`

export const GET_MOVIE = gql`
  query GetMovie($id: String) {
    movie(_id: $id) {
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }
  }
`

export const EDIT_MOVIE = gql`
  mutation EditMovie($id: String, $movie: newMovie) {
    editMovie(id: $id, movie: $movie) {
      msg
    }
  }
`

export const DELETE_MOVIE = gql`
  mutation DeleteMovie($id: String) {
    deleteMovie(id: $id) {
      msg
    }
  }
`

export const GET_FAVORITES = gql`
  query getFavorites{
    favorites{
      _id,
      title,
      overview,
      poster_path,
      popularity,
      tags
    }
  }
`