import { ApolloClient, InMemoryCache } from '@apollo/client';
import { GET_FAVORITES } from '../query'

const client = new ApolloClient({
  uri: "http://localhost:5000/",
  cache: new InMemoryCache()
})

client.writeQuery({
  query: GET_FAVORITES,
  data: {
    favorites: []
  }
})

export default client