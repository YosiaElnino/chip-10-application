const Movie = require('../models/movie')

class MovieController {
  static async create (req, res) {
    Movie.create(req.body, (err, data) => {
      if (data) {
        res.status(201).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async read (req, res) {
    Movie.findAll((err, data) => {
      if (data) {
        res.status(200).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async findOne (req, res) {
    Movie.findOne(req.params.id, (err, data) => {
      if (data) {
        res.status(200).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async update (req, res) {
    Movie.update(req.params.id, req.body, (err, data) => {
      if (data) {
        res.status(200).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async delete (req, res) {
    Movie.delete(req.params.id, (err, data) => {
      if (data) {
        res.status(200).json({
          msg: 'Item has been deleted'
        })
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }
}

module.exports = MovieController