const { ObjectId } = require('mongodb')
const db = require('../config/connection')

class Movie {
  static async create(movie, cb) {
    try {
      const { title, overview, poster_path, popularity, tags } = movie
      const MovieCollection = db.collection('movies')
      const result = await MovieCollection.insertOne({
        title, overview, poster_path, popularity, tags
      })
      cb(null, result.ops)
    } catch (error) {
      cb (error, null)
    }
  }

  static async findAll(cb) {
    try {
      const MovieCollection = db.collection('movies')
      const data = await MovieCollection.find().toArray()
      cb(null, data)
    } catch (error) {
      cb(error, null)
    }
  }

  static async findOne(id, cb) {
    try {
      const MovieCollection = db.collection('movies')
      const filter = {
        _id: ObjectId(id)
      }
      const data = await MovieCollection.find(filter).toArray()
      if (data.length) {
        cb(null, data[0])
      } else {
        throw {msg: 'Not Found', status: 404}
      }
    } catch (error) {
      cb(error, null)
    }
  }

  static async update(id, movie, cb) {
    try {
      const { title, overview, poster_path, popularity, tags } = movie
      const MovieCollection = db.collection('movies')
      const filter = {
        _id: ObjectId(id)
      }
      const updateData = { $set: { title, overview, poster_path, popularity, tags } }
      const result = await MovieCollection.updateOne(filter, updateData)
      cb(null, { msg: 'Data has been updated'})
    } catch (error) {
      cb(error, null)
    }
  }

  static async delete(id, cb) {
    try {
      const MovieCollection = db.collection('movies')
      const filter = {
        _id: ObjectId(id)
      }
      const result = await MovieCollection.deleteOne(filter)
      cb(null, result)
    } catch (error) {
      cb(error, null)
    }
  }
}

module.exports = Movie