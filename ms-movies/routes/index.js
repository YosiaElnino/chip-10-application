const router = require('express').Router()
const MovieController = require('../controllers/movieController')

router.get('/', MovieController.read)
router.get('/:id', MovieController.findOne)
router.post('/', MovieController.create)

router.put('/:id', MovieController.update)
router.delete('/:id', MovieController.delete)

module.exports = router