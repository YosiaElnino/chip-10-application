var apm = require('elastic-apm-node').start({
  serviceName: 'gql-orchestrator',
  secretToken: '',
  serverUrl: 'http://apm-server:8200',

  logLevel:"debug",
  captureBody: 'all',
  active: true,
  usePathAsTransactionName: true,

  environment: 'production'
})

const { ApolloServer, gql, makeExecutableSchema } = require('apollo-server')
const movieSchema = require('./schemas/movieSchema')
const seriesSchema = require('./schemas/seriesSchema')

const typeDefs = gql`
  type Query
  type Mutation
`

const schema = makeExecutableSchema({
  typeDefs : [
    typeDefs,
    movieSchema.typeDefs,
    seriesSchema.typeDefs
  ],
  resolvers : [
    movieSchema.resolvers,
    seriesSchema.resolvers
  ]
})

const server = new ApolloServer({
  schema
})

server.listen(5000).then(({ url }) => {
  console.log(`Server is running at ${url}`)
})