const { gql } = require('apollo-server');
const axios = require('axios');
//const Redis = require('ioredis')
//const redis = new Redis()

const moviesUrl = 'http://ms-movies:5001/'

const typeDefs = gql`
type Movie {
  _id : String,
  title : String,
  overview : String,
  poster_path : String,
  popularity : Float,
  tags : [String]
}

type MovieResponse {
  msg: String
}

extend type Query {
  movies: [Movie],
  movie(_id: String) : Movie
}

input newMovie {
  title : String,
  overview : String,
  poster_path : String,
  popularity : Float,
  tags : [String]
}

extend type Mutation {
  addMovie(movie: newMovie) : Movie
  editMovie(id: String, movie: newMovie) : MovieResponse
  deleteMovie(id: String) : MovieResponse
}
`

const resolvers = {
  Query: {
    movies: async() => {
      //const moviesCache = JSON.parse(await redis.get('movies'))
      //if (moviesCache) return moviesCache
      return axios({
        method: 'GET',
        url: moviesUrl
      })
        .then(({ data }) => {
          // redis.set('movies', JSON.stringify(data))
          return data
        })
        .catch(console.log)
    },
    movie: async(parent, args, context, info) => {
      const {_id} = args
      //const moviesCache = JSON.parse(await redis.get('movies'))
      //if (moviesCache) return moviesCache.find(el => el._id === _id)
      return axios({
        url: moviesUrl + _id,
        method: 'GET'
      })
        .then(({ data }) => {
          return data
        })
        .catch(console.log)
    }
  },
  Mutation: {
    addMovie: async(_, args) => {
      const { title, overview, poster_path, popularity, tags } = args.movie
      //const moviesCache = JSON.parse(await redis.get('movies'))
      return axios({
        method: 'POST',
        url: moviesUrl,
        data: { title, overview, poster_path, popularity, tags }
      })
        .then(({ data }) => {
          // if (moviesCache) {
          //   moviesCache.push(data[0])
          //   redis.set('movies', JSON.stringify(moviesCache))
          // }
          return data[0]
        })
        .catch(console.log)
    },
    editMovie: async(_, args) => {
      const { id } = args
      const { title, overview, poster_path, popularity, tags } = args.movie
      //const moviesCache = JSON.parse(await redis.get('movies'))
      return axios({
        url: moviesUrl + id,
        method: 'PUT',
        data: { title, overview, poster_path, popularity, tags }
      })
        .then(({ data }) => {
          if (moviesCache) {
            const result = moviesCache.map(element => {
              if (element._id === id) {
                element.title = title,
                element.overview = overview,
                element.poster_path = poster_path,
                element.popularity = popularity,
                element.tags = tags
              }
              return element
            })
            //redis.set('movies', JSON.stringify(result))
          }
          return data
        })
        .catch(console.log)
    },
    deleteMovie: async(_, args) => {
      const { id } = args
      //const moviesCache = JSON.parse(await redis.get('movies'))
      return axios({
        url: moviesUrl + id,
        method: 'DELETE'
      })
        .then(({ data }) => {
          // if (moviesCache) {
          //   const result = moviesCache.filter(el => el._id !== id)
          //   redis.set('movies', JSON.stringify(result))
          // }
          return data
        })
        .catch(console.log)
    }
  }
}

module.exports = {
  typeDefs,
  resolvers
}