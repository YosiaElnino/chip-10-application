const { gql } = require('apollo-server');
const axios = require('axios');
//const Redis = require('ioredis')
//const redis = new Redis()

const seriesUrl = 'http://ms-series:5002/'

const typeDefs = gql`
type Series {
  _id : String,
  title : String,
  overview : String,
  poster_path : String,
  popularity : Float,
  tags : [String]
}

type SeriesResponse {
  msg: String
}

extend type Query {
  series: [Series],
  oneSeries(_id: String!) : Series
}

input newSeries {
  title : String,
  overview : String,
  poster_path : String,
  popularity : Float,
  tags : [String]
}

extend type Mutation {
  addSeries(series: newSeries) : Series
  editSeries(id: String, series: newSeries) : SeriesResponse
  deleteSeries(id: String) : SeriesResponse
}
`

const resolvers = {
  Query: {
    series: async() => {
      //const seriesCache = JSON.parse(await redis.get('series'))
      //if (seriesCache) return seriesCache
      return axios({
        method: 'GET',
        url: seriesUrl
      })
        .then(({ data }) => {
          //redis.set('series', JSON.stringify(data))
          return data
        })
        .catch(console.log)
    },
    oneSeries: async(parent, args, context, info) => {
      const {_id} = args
      //const seriesCache = JSON.parse(await redis.get('series'))
      //if (seriesCache) return seriesCache.find(el => el._id === _id)
      return axios({
        url: seriesUrl + _id,
        method: 'GET'
      })
        .then(({ data }) => {
          return data
        })
        .catch(console.log)
    }
  },
  Mutation: {
    addSeries: (_, args) => {
      const { title, overview, poster_path, popularity, tags } = args.series
      return axios({
        method: 'POST',
        url: seriesUrl,
        data: { title, overview, poster_path, popularity, tags }
      })
        .then(({ data }) => {
          return data[0]
        })
        .catch(console.log)
    },
    editSeries: (_, args) => {
      const { id } = args
      const { title, overview, poster_path, popularity, tags } = args.series
      return axios({
        url: seriesUrl + id,
        method: 'PUT',
        data: { title, overview, poster_path, popularity, tags }
      })
        .then(({ data }) => {
          return data
        })
        .catch(console.log)
    },
    deleteSeries: (_, args) => {
      const { id } = args
      return axios({
        url: seriesUrl + id,
        method: 'DELETE'
      })
        .then(({ data }) => {
          return data
        })
        .catch(console.log)
    }
  }
}

module.exports = {
  typeDefs,
  resolvers
}