const Series = require('../models/Series')

class SeriesController {
  static async create (req, res) {
    Series.create(req.body, (err, data) => {
      if (data) {
        res.status(201).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async read (req, res) {
    Series.findAll((err, data) => {
      if (data) {
        res.status(200).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async findOne (req, res) {
    Series.findOne(req.params.id, (err, data) => {
      if (data) {
        res.status(200).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async update (req, res) {
    Series.update(req.params.id, req.body, (err, data) => {
      if (data) {
        res.status(200).json(data)
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }

  static async delete (req, res) {
    Series.delete(req.params.id, (err, data) => {
      if (data) {
        res.status(200).json({
          msg: 'Item has been deleted'
        })
      } else {
        res.status(400).json({
          msg: 'Internal server error'
        })
      }
    })
  }
}

module.exports = SeriesController