const { ObjectId } = require('mongodb')
const db = require('../config/connection')

class Series {
  static async create(series, cb) {
    try {
      const { title, overview, poster_path, popularity, tags } = series
      const SeriesCollection = db.collection('series')
      const result = await SeriesCollection.insertOne({
        title, overview, poster_path, popularity, tags
      })
      cb(null, result.ops)
    } catch (error) {
      cb (error, null)
    }
  }

  static async findAll(cb) {
    try {
      const SeriesCollection = db.collection('series')
      const data = await SeriesCollection.find().toArray()
      cb(null, data)
    } catch (error) {
      cb(error, null)
    }
  }

  static async findOne(id, cb) {
    try {
      const SeriesCollection = db.collection('series')
      const filter = {
        _id: ObjectId(id)
      }
      const data = await SeriesCollection.find(filter).toArray()
      if (data.length) {
        cb(null, data[0])
      } else {
        throw {msg: 'Not Found', status: 404}
      }
    } catch (error) {
      cb(error, null)
    }
  }

  static async update(id, series, cb) {
    try {
      const { title, overview, poster_path, popularity, tags } = series
      const SeriesCollection = db.collection('series')
      const filter = {
        _id: ObjectId(id)
      }
      const updateData = { $set: { title, overview, poster_path, popularity, tags } }
      const result = await SeriesCollection.updateOne(filter, updateData)
      cb(null, { msg: 'Data has been updated'})
    } catch (error) {
      cb(error, null)
    }
  }

  static async delete(id, cb) {
    try {
      const SeriesCollection = db.collection('series')
      const filter = {
        _id: ObjectId(id)
      }
      const result = await SeriesCollection.deleteOne(filter)
      cb(null, result)
    } catch (error) {
      cb(error, null)
    }
  }
}

module.exports = Series