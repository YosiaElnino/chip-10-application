const { MongoClient } = require('mongodb')
const databaseUrl = 'mongodb://root:password@db-mongo:27017'

const client = MongoClient( databaseUrl, { useUnifiedTopology: true })

client.connect()

const db = client.db('entertain-me')

module.exports = db