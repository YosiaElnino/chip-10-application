var apm = require('elastic-apm-node').start({
    serviceName: 'ms-series',
    secretToken: '',
    serverUrl: 'http://apm-server:8200',

    logLevel:"debug",
    captureBody: 'all',
    active: true,
    usePathAsTransactionName: true,

    environment: 'production'
  })

const express = require('express');
const routes = require('./routes/index')

const app = express()
const PORT = 5002
app.use(express.json())

app.use(routes)

app.listen(PORT, () => console.log(`app is listening at http"//localhost:${PORT}`))